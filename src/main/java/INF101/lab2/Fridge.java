package INF101.lab2;
import java.util.ArrayList;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    
    ArrayList<FridgeItem> myFridge = new ArrayList<>();
    
        public int nItemsInFridge(){
            return myFridge.size();
        }
        public int totalSize(){
            return 20;
        }
        public boolean placeIn(FridgeItem item){
            if (myFridge.size()<20){
                myFridge.add(item);
            return true;
            }
            else{
            return false;
            }
        }
        public void takeOut(FridgeItem item){
            if (myFridge.size()>0 && myFridge.contains(item)){
                myFridge.remove(item);
            }
            else{
               throw new NoSuchElementException();
            }
        }
        public void emptyFridge(){
            myFridge.clear();
        }
        public ArrayList<FridgeItem> removeExpiredFood(){

            ArrayList<FridgeItem> removedItems = new ArrayList<FridgeItem>();

            for (FridgeItem item : myFridge) {
                if (item.hasExpired()) {
                    removedItems.add(item);
                }
            }
            for (FridgeItem item : removedItems) {
                takeOut(item);
            }
            return removedItems;       
        } 
}

